FROM maven:3.8.5-openjdk-17-slim
COPY target/*.jar .
COPY datasets/uf.csv ./datasets/
EXPOSE 8080
CMD ["java", "-jar", "UF-0.0.1-SNAPSHOT.jar"]