package dci.ufro.cl.UF.util;


import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import dci.ufro.cl.UF.model.Uf;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class SmartCsvWriter {

    public void writeUf() throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, InterruptedException {
        Writer writer = new FileWriter("datasets/uf.csv");

        StatefulBeanToCsv<Uf> beanToCsv = new StatefulBeanToCsvBuilder<Uf>(writer)
                .withSeparator(';')
                .withLineEnd(CSVWriter.DEFAULT_LINE_END)
                .withOrderedResults(true)
                .build();
    }
}