package dci.ufro.cl.UF.util;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import dci.ufro.cl.UF.model.Uf;
import dci.ufro.cl.UF.model.UfPorFecha;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CSVReader {

    public List<UfPorFecha> readCsvFile() throws FileNotFoundException, ParseException {
        FileReader reader = new FileReader("datasets/uf.csv");
        CsvToBean<Uf> csvtobean = new CsvToBeanBuilder<Uf>(reader)
                .withType(Uf.class)
                .withSeparator(';')
                .build();
        List<Uf> lista = csvtobean.parse();
        List<UfPorFecha> ufPorFechas = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {

            if (i > 0) {
                String[] cadenas = lista.get(i).getDate().split("/");
                int day = Integer.parseInt(cadenas[0]);
                int month = Integer.parseInt(cadenas[1]);
                int annio = Integer.parseInt(cadenas[2]);
                LocalDate date = LocalDate.of(annio, month, day);
                String a = lista.get(i).getUF().replace(",","");
                double str1 = Double.parseDouble(a);
                UfPorFecha ufPorFecha = new UfPorFecha(date, str1);
                ufPorFechas.add(ufPorFecha);
            }

        }
        return ufPorFechas;
    }
}
