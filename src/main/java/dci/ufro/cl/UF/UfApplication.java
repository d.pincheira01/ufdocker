package dci.ufro.cl.UF;

import dci.ufro.cl.UF.util.CSVReader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileNotFoundException;
import java.text.ParseException;

@SpringBootApplication
public class UfApplication {

    public static void main(String[] args) throws FileNotFoundException, ParseException {
        SpringApplication.run(UfApplication.class, args);
    }
}
