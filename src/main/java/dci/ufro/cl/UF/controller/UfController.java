package dci.ufro.cl.UF.controller;


import dci.ufro.cl.UF.model.Uf;
import dci.ufro.cl.UF.model.UfPorFecha;
import dci.ufro.cl.UF.util.CSVReader;
import dci.ufro.cl.UF.util.SmartCsvWriter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.time.Month;
import java.util.List;


@RestController
@RequestMapping("/uf")
public class UfController {

    @GetMapping("/promedioAgosto1999")
    public String promedioAgo1999() throws FileNotFoundException, ParseException {
        CSVReader c = new CSVReader();
        List<UfPorFecha> ufPorFechas = c.readCsvFile();
        double promedioAgosto = 0;
        int acum = 0;
        for (UfPorFecha u : ufPorFechas) {
            if (u.getFecha().getYear() == 1999 && u.getFecha().getMonth() == Month.AUGUST) {
                promedioAgosto = promedioAgosto + u.getPrecio();
                acum++;
            }
        }
        promedioAgosto = promedioAgosto / acum;
        String resp = "El promedio de la uf en agosto de 1999 es: " + promedioAgosto;

        return resp;
    }

    @GetMapping("/promedioMeses2010")
    public String promedioMeses2010() throws FileNotFoundException, ParseException {
        CSVReader c = new CSVReader();
        List<UfPorFecha> ufPorFechas = c.readCsvFile();
        double promedioMeses = 0;
        int acum = 0;
        for (UfPorFecha u : ufPorFechas) {
            if (u.getFecha().getYear() == 2010) {
                promedioMeses = promedioMeses + u.getPrecio();
                acum++;
            }
        }
        promedioMeses = promedioMeses / acum;
        String resp = "El promedio de la UF en los meses del año 2010 es: " + promedioMeses;

        return resp;
    }
}



