package dci.ufro.cl.UF.model;

import java.time.LocalDate;
import java.util.Date;

public class UfPorFecha {
    private LocalDate fecha;
    private double precio;

    public UfPorFecha(LocalDate fecha, double precio) {
        this.fecha = fecha;
        this.precio = precio;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
